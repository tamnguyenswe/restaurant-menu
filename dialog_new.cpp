#include    "menu/dialog.cpp"

#include    "global_variables.h"
#include    "global_functions.cpp"

#ifndef _DIALOG_NEW_
#define _DIALOG_NEW_

void Dialog::show_order(MenuItem *this_item) {
    const int   BTN_BACK        =   0,
                BTN_DECREASE    =   1,
                BTN_INCREASE    =   2,
                BTN_OK          =   3;

    this->title = this_item->title;
    this->message = "Amount: " + std::to_string(this_item->order_amount);

    alignButton();
    selectedButton = -1;
    cursor = 0;
    visible = true;

    draw();
    wait_button(BTN_4);

    int old_amount = this_item->order_amount;
    while (visible) {
        if(is_pressed(BTN_1)) {
            wait_button(BTN_1);
            back();
        }
        if(is_pressed(BTN_2)) {
            wait_button(BTN_2);
            up();
        }
        if(is_pressed(BTN_3)) {
            wait_button(BTN_3);
            down();
        }
        if(is_pressed(BTN_4)) {
            wait_button(BTN_4);

            switch (cursor) {
                case BTN_BACK:
                    this_item->order_amount = old_amount;
                    visible = false;
                    break;
                case BTN_DECREASE:
                    if (this_item->order_amount > 0) {
                        this_item->order_amount--;
                        this->message = "Amount: " + std::to_string(this_item->order_amount);
                        draw();
                    }
                    break;
                case BTN_INCREASE:
                    this_item->order_amount++;
                    this->message = "Amount: " + std::to_string(this_item->order_amount);
                    draw();
                    break;
                case BTN_OK:
                    visible = false;
                    break;
            }
        }
    }
    clearButton();
    // LCDsetTinyFont(false);
    LCD_inner->LCDsetFont(font5x5);
}

#endif