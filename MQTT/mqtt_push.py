# MQTT Publish Demo
# Publish two messages, to two different topics

import paho.mqtt.publish as publish
import sys

qr_string = sys.argv[1]
ip_server = sys.argv[2]
publish.single("/foo/bar", qr_string, hostname=ip_server)
# publish.single("foo/topic", "World!", hostname="192.168.1.102")
# print("Done")