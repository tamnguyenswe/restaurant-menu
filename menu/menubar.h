#ifndef MENUBAR_H
#define MENUBAR_H

#include <iostream>
#include <vector>
#include <PCD8544.h>

#include "menuitem.h"
#include "dialog.cpp"

#define ITEM_START_POS 13
#define ITEM_SIZE 8

using namespace std;

class MenuBar
{
public:
    MenuBar(string title = "Main Menu");
    void addItem(MenuItem *item);
    void setTitle(string title);
    void enter();
    void up();
    void down();
    void back();
    void update();
    MenuItem root;
    MenuItem* currentMenu;
    bool is_inited;
    int showDialog(string title="Dialog", string message="Message", int flag = MESSAGE_DIALOG, int duration = 2000);

protected:
    int cursorPos = 0;
    int windowPos = 0;
    Dialog dialog;

};

#endif // MENUBAR_H
