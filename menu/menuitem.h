#ifndef MENUITEM_H
#define MENUITEM_H

#include <vector>
#include <string>
#include <functional>

using namespace std;

class MenuItem {
    public:
        MenuItem(string title = "", int = 0, int = 0);
        void addItem(MenuItem *item);
        void setTitle(string title);
        void select();
        void setAction(function<void()> cb);
        string getTitle();

        vector<MenuItem *> getItems() const;
        MenuItem* parent;
        vector<MenuItem*> items;
        string title;

        int order_amount = 0;
        int price = 0;
        int index;
    protected:
        // void (*)() = nullptr;
        function <void()> callback = [](){};
};

#endif // MENUITEM_H
