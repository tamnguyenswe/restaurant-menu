#include <wiringPi.h>
#include <string.h>
#include <PCD8544.h>
#include <sstream>
#include <vector>

#include "../global_variables.h"
#include "../global_functions.cpp"

bool light = true;
bool is_done;

void menu_quit() {
    is_done = true;
    is_quit = true;
}

void clear_review() {
    LCD_inner->LCDfillrect(2, 12, 80, 34, WHITE);
}

void send_mqtt_qr_string(std::string qr_string) {
    std::ostringstream sstr;

    sstr << "python ./MQTT/mqtt_push.py " << "\"" << qr_string << "\" " << SERVER_ADDRESS;

    std::string mqtt_payload = sstr.str();

    system(mqtt_payload.c_str());
    std::cout << mqtt_payload << std::endl;
}

void order_review() {
    std::ostringstream string_stream;
    std::vector<string> order_in_lines;
    int total_pay_all_orders = 0;

    for (auto const& order : order_list) {
        int total_pay_this_dish = order.second.amount * order.second.price;

        //clear string stream
        string_stream.str(std::string());

        string_stream << order.first << "x" << order.second.amount << "=" << total_pay_this_dish << std::endl;

        total_pay_all_orders += total_pay_this_dish;

        order_in_lines.push_back(string_stream.str());
    }

    //clear string stream
    string_stream.str(std::string());

    string_stream << "Total: " << total_pay_all_orders << std::endl; 
    order_in_lines.push_back(string_stream.str());

    std::string title_review = "REVIEW";
    LCD_inner->LCDclear();

    LCD_inner->LCDsetTextColor(WHITE);
    LCD_inner->LCDfillrect(0, 0, 84, 11, BLACK);
    LCD_inner->LCDdrawstring_P((84 - title_review.size() * 6) / 2, 2, title_review.c_str());
    LCD_inner->LCDdrawrect(0, 0, 84, 48, BLACK);

    LCD_inner->LCDsetTextColor(BLACK);

    int moc = 0;
    clear_review();

    for (int i = 0; (i < order_in_lines.size()) && (i < 3); i++) {
        LCD_inner->LCDdrawstring_P(4, 14 + i*8, order_in_lines[i].c_str());
        delay(50);
    }

    
    LCD_inner->LCDdisplay();

    delay(100);
    LCD_inner->LCDdisplay();

    moc = 3;
    while (1) {
        if (is_pressed(BTN_1)) {
            wait_button(BTN_1);

            menuBar.update();
            break;
        }

        if (is_pressed(BTN_2)) {
            wait_button(BTN_2);
            clear_review();

            moc -= 6;
            if (moc < 0) moc = 0;

            for (int i = moc; (i < order_in_lines.size()) && (i < moc + 3); i++) {
                LCD_inner->LCDdrawstring_P(4, 14 + (i-moc)*8, order_in_lines[i].c_str());
                delay(50);
            }       
            
            moc += 3;     
            LCD_inner->LCDdisplay();
        }

        if (is_pressed(BTN_3)) {
            wait_button(BTN_3);
            clear_review();

            for (int i = moc; (i < order_in_lines.size()) && (i < moc + 3); i++) {
                LCD_inner->LCDdrawstring_P(4, 14 + (i-moc)*8, order_in_lines[i].c_str());
                delay(50);
            }
            moc += 3;
            LCD_inner->LCDdisplay();
        }
    }
}

void show_qr_code() {

    std::ostringstream string_stream;
    std::string qr_string;

    string_stream << "T1"; //Table ID, T1 = Table 1

    for (auto const& order : order_list) {
        string_stream << "#" << order.second.index << "x" << order.second.amount;
    }

    qr_string = string_stream.str();

    //send qr string through mqtt
    send_mqtt_qr_string(qr_string);

    //generate qr code
    uint8_t qrcodeBytes[qrcode_getBufferSize(5)];
    qrcode_initText(&qrcode, qrcodeBytes, 5, ECC_LOW, qr_string.c_str());

    LCD_inner->LCDclear();
    LCD_outer->LCDclear();

    LCD_inner->LCDdrawrect(0, 0, 84, 48, BLACK);
    LCD_outer->LCDdrawrect(0, 0, 84, 48, BLACK);

    const int   PADDING_HORIZONTAL  =   24,
                PADDING_VERTICAL    =   6;

    for (int y = 0; y < qrcode.size; y++) {
        for (int x = 0; x < qrcode.size; x++) {
            if (qrcode_getModule(&qrcode, x, y)) {
                LCD_inner->LCDsetPixel(x + PADDING_HORIZONTAL, y + PADDING_VERTICAL, BLACK);
                LCD_outer->LCDsetPixel(x + PADDING_HORIZONTAL, y + PADDING_VERTICAL, BLACK);
            } else {
                LCD_inner->LCDsetPixel(x + PADDING_HORIZONTAL, y + PADDING_VERTICAL, WHITE);
                LCD_outer->LCDsetPixel(x + PADDING_HORIZONTAL, y + PADDING_VERTICAL, WHITE);
            }
        }
    }

    delay(100);
    LCD_inner->LCDdisplay();
    LCD_outer->LCDdisplay();

    while (1) {
        if (is_pressed(BTN_1)) {
            wait_button(BTN_1);
            break;
        }
    }

    menuBar.update();
}

void menu_init() {
    MenuItem *mi_fish       =   new MenuItem("Fish");
    MenuItem *mi_rice       =   new MenuItem("Rice");
    MenuItem *mi_pizza      =   new MenuItem("Pizza");
    MenuItem *quit          =   new MenuItem("Quit");
    MenuItem *mi_review     =   new MenuItem("Review");
    MenuItem *mi_show_qr    =   new MenuItem("Show QR");

    MenuItem *fish1         =   new MenuItem("Fish 1", 30, 1);
    MenuItem *fish2         =   new MenuItem("Fish 2", 40, 2);

    MenuItem *rice1         =   new MenuItem("Rice 1", 20, 3);
    MenuItem *rice2         =   new MenuItem("Rice 2", 80, 4);

    MenuItem *pizza1        =   new MenuItem("Pizza 1", 35, 5);
    MenuItem *pizza2        =   new MenuItem("Pizza 2", 15, 6);

    mi_fish->addItem(fish1);
    mi_fish->addItem(fish2);

    mi_rice->addItem(rice1);
    mi_rice->addItem(rice2);

    mi_pizza->addItem(pizza1);
    mi_pizza->addItem(pizza2);

    menuBar.addItem(mi_fish);
    menuBar.addItem(mi_rice);
    menuBar.addItem(mi_pizza);
    menuBar.addItem(mi_review);
    menuBar.addItem(mi_show_qr);
    menuBar.addItem(quit);

    quit->setAction(menu_quit); 
    mi_review->setAction(order_review);
    mi_show_qr->setAction(show_qr_code);

    LCD_inner->LCDclear();
    delay(50);
    LCD_inner->LCDdisplay();
}

void menu_show() {
    is_done     =   false;

    menuBar.update();

    while (!is_done) {

        if(is_pressed(BTN_1)) {
            wait_button(BTN_1);

            // if (menuBar.currentMenu->getTitle() == root.currentMenu->getTitle()) {
            //     is_done = true;
            // }
            // if (menuBar.currentMenu->parent == NULL) {
            //     is_done = true;
            // }
            
            menuBar.back();
        }
        if(is_pressed(BTN_2)) {
            wait_button(BTN_2);

            menuBar.up();
        }
        if(is_pressed(BTN_3)) {
            wait_button(BTN_3);

            menuBar.down();
        }

        if(is_pressed(BTN_4)) {
            wait_button(BTN_4);

            menuBar.enter();
        }
    }
}