#ifndef MENU_H
#define	MENU_H

#include <wiringPi.h>
#include <string.h>
#include <PCD8544.h>

#include "menubar.cpp"
#include "menuitem.cpp"
#include "../global_functions.cpp"
#include "../global_variables.h"

void initPin();
void helloWorld();
void changeLight();
void showLogo();
void menu_show();
void menu_init();
void menu_init();

#endif