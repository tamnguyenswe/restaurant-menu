#include "menu/menubar.cpp"
// #include "sstream"
#include "global_variables.h"
#include "global_functions.cpp"

#ifndef _MENU_BAR_NEW_
#define _MENU_BAR_NEW_

class RestaurantMenuBar : public MenuBar {
    public:
        RestaurantMenuBar(string title) : MenuBar(title) {
        }

        void up() {
            MenuBar::up();

            // if (cursorPos > 0) cursorPos--;
         //    else cursorPos = currentMenu->getItems().size() - 1;

         //    if (cursorPos < windowPos) windowPos = cursorPos;
         //    if (cursorPos > windowPos + 1) windowPos = cursorPos - 1;

            update();
        }

        void down() {
            MenuBar::down();

            // if (cursorPos < currentMenu->getItems().size() - 1) cursorPos++;
            // else cursorPos = 0;

            // if (cursorPos < windowPos) windowPos = cursorPos;
            // if (cursorPos > windowPos + 1) windowPos = cursorPos - 1;

            update();
        }

        void back() {
            MenuBar::back();
            update();
        }

        void enter() {
            MenuItem *this_item = currentMenu->getItems()[cursorPos]; 

            if (this_item->getItems().size() == 0) {

                if (is_keyword(this_item->getTitle())) {
                    MenuBar::enter();
                    return;
                } else {
                    // set_order_amount(this_item);
                    show_order_dialog(this_item);
                }

            } else {
                MenuBar::enter();
            }

            delay(100);
            update();
        }

        void update() {
            LCD_inner->LCDclear();

            // draw title
            LCD_inner->LCDsetTextColor(WHITE);
            LCD_inner->LCDfillrect(0, 0, 84, 11, BLACK);
            LCD_inner->LCDdrawstring_P((84 - currentMenu->getTitle().size() * 6) / 2, 2, currentMenu->getTitle().c_str());

            // draw background cursor
            LCD_inner->LCDfillrect(2, ITEM_START_POS + ITEM_SIZE * (cursorPos - windowPos), 80, 9, BLACK);

            // draw item

            for (int i = windowPos; i < min(windowPos + 4, currentMenu->getItems().size()); i++) {

                MenuItem *this_item = currentMenu->getItems()[i];

                string text =   this_item->getTitle();
                int amount  =   this_item->order_amount;

               

                if (text.size() > 13) text = text.substr(0, 13);

                LCD_inner->LCDsetTextColor(BLACK);

                if (i == cursorPos) {
                    LCD_inner->LCDsetTextColor(WHITE);
                    LCD_inner->LCDdrawstring_P(3, ITEM_START_POS + ITEM_SIZE * (i - windowPos) + 1, text.c_str());

                    if (this_item->getItems().size() <= 0) {
                        print_order_amount(i, this_item->order_amount, this_item);
                    }
                }
                else if (i > cursorPos) {
                    LCD_inner->LCDdrawstring_P(3, ITEM_START_POS + ITEM_SIZE * (i - windowPos) + 2, text.c_str());
                    if (this_item->getItems().size() <= 0) {
                        print_order_amount(i, this_item->order_amount, this_item);
                    }
                }
                else {
                    LCD_inner->LCDdrawstring_P(3, ITEM_START_POS + ITEM_SIZE * (i - windowPos), text.c_str());

                    if (this_item->getItems().size() <= 0) {
                        print_order_amount(i, this_item->order_amount, this_item);
                    }
                }
            }


            LCD_inner->LCDdrawrect(0, 0, 84, 48, BLACK);
            LCD_inner->LCDsetTextColor(BLACK);

            delay(50);
            LCD_inner->LCDdisplay();
        }

        void show_order_dialog(MenuItem *this_item) {
            dialog.clearButton();
            dialog.addButton(new Button("", 0, 0, CANCEL, CANCEL_HOVER));
            dialog.addButton(new Button("", 0, 0, DECREASE, DECREASE_HOVER));
            dialog.addButton(new Button("", 0, 0, INCREASE, INCREASE_HOVER));
            dialog.addButton(new Button("", 0, 0, ACCEPT, ACCEPT_HOVER));
            dialog.show_order(this_item);
            push_to_order_list(this_item);
        }   

    protected:

        int get_amount_position(std::string amount) {
            int DEFAULT = 65;
            int CHAR_WIDTH = 6;
            
            int length = amount.length() - 3; //2 char "<" va ">"

            return DEFAULT - CHAR_WIDTH * length;
        }

        void print_order_amount(int i, int amount, MenuItem *this_item) {
            if (is_keyword(this_item->getTitle())) {
                return;
            }

            std::ostringstream string_stream;
            string_stream << "<" << amount << ">";
            string change_amount = string_stream.str();

            LCD_inner->LCDdrawstring_P(get_amount_position(change_amount), ITEM_START_POS + ITEM_SIZE * (i - windowPos) + 1 , change_amount.c_str());
        } 

        void set_order_amount(MenuItem *this_item) {
            int old_amount = this_item->order_amount;

            while(1) {
                if (is_pressed(BTN_1)) {
                    wait_button(BTN_1);
                    this_item->order_amount = old_amount;
                    break;
                }

                if (is_pressed(BTN_2)) {
                    wait_button(BTN_2);
                    if (this_item->order_amount > 0) {
                        this_item->order_amount--;
                        delay(50);
                        update();
                    }
                }

                if (is_pressed(BTN_3)) {
                    wait_button(BTN_3);
                    this_item->order_amount++;
                    delay(50);
                    update();                       
                }

                if (is_pressed(BTN_4)) {
                    wait_button(BTN_4);
                    
                    int total_pay = (this_item->order_amount * this_item->price);

                    push_to_order_list(this_item);

                    while (1) {
                        if (is_pressed(BTN_1)) {
                            wait_button(BTN_1);

                            break;
                        }

                        if (is_pressed(BTN_4)) {
                            wait_button(BTN_4);

                            LCD_inner->LCDfillrect(2, 12, 80, 34, WHITE);
                            // LCD_inner->LCDdrawrect(3, 12, 78, 34, BLACK);

                            delay(50);
                            LCD_inner->LCDdisplay();

                        }
                    }

                    break;
                }
            }           
        }

    private:
        bool is_keyword(std::string title) {
            for (int i = 0; i < KEYWORD_SIZE; i++) {
                if (title == keywords[i]) {
                    return true;
                }
            }

            return false;
        }

        void push_to_order_list(MenuItem *this_item) {
            struct order_info info;

            info.amount = this_item->order_amount;
            info.price  = this_item->price;
            info.index  = this_item->index;

            if (info.amount > 0) {
                order_list[this_item->title] = info; //neu >0 thi moi cho vao map, khong thi bo qua
            }
        }

};

#endif