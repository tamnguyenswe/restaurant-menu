#include <stdint.h>

#define     BLACK 1
#define     WHITE 0

#define     LCDWIDTH    84
#define     LCDHEIGHT   48

#define     PCD8544_POWERDOWN           0x04
#define     PCD8544_ENTRYMODE           0x02
#define     PCD8544_EXTENDEDINSTRUCTION 0x01

#define     PCD8544_DISPLAYBLANK    0x0
#define     PCD8544_DISPLAYNORMAL   0x4
#define     PCD8544_DISPLAYALLON    0x1
#define     PCD8544_DISPLAYINVERTED 0x5

// H = 0
#define     PCD8544_FUNCTIONSET     0x20
#define     PCD8544_DISPLAYCONTROL  0x08
#define     PCD8544_SETYADDR        0x40
#define     PCD8544_SETXADDR        0x80

// H = 1
#define     PCD8544_SETTEMP     0x04
#define     PCD8544_SETBIAS     0x10
#define     PCD8544_SETVOP      0x80

// #define     swap(a, b) { uint8_t t = a; a = b; b = t; }
#define     abs(a) (((a) < 0) ? -(a) : (a))

// bit set
#define     _BV(bit) (0x1 << (bit))

 // calibrate clock constants
#define     CLKCONST_1  8000
#define     CLKCONST_2  400  // 400 is a good tested value for Raspberry Pi

// keywords
#define     LSBFIRST  0
#define     MSBFIRST  1

//default values
#define     DEFAULT_SCLK        14
#define     DEFAULT_DIN         12
#define     DEFAULT_DC          4
#define     DEFAULT_CS          10
#define     DEFAULT_RST         5
#define     DEFAULT_CONTRAST    50

#define     FONT_INFO_RESERVED  4   // 4 first value of each font, containing those information: font's width, height, first char, number of chars

#define     MAGIC_DEBUG_VALUE   0.01 // increase in case of bug
