#ifndef _GLOBAL_VARS_
#define _GLOBAL_VARS_
	
	
    #include    <map>
	#include 	<sstream>
	#include	"./QRCode/src/qrcode.c"

	#include	"./PCD8544/PCD8544.cpp"
	#include	"./PCD8544/Fonts/font5x5.c"
	LCD *LCD_inner;
	LCD *LCD_outer;

	#define		EXTENDED_SCLK	1
	#define		EXTENDED_DIN	27
	#define		EXTENDED_DC		24
	#define		EXTENDED_CS		23
	#define		EXTENDED_RTS	26

	#define 	BTN_1 7
	#define 	BTN_2 0
	#define 	BTN_3 21
	#define 	BTN_4 22
	#define 	BACKLIGHT 30

	#define		ON HIGH
	#define		OFF !ON

	#define		KEYWORD_SIZE	4

    #include    "./menu/menuitem.cpp"
    #include    "./menu/menubar.cpp"

	bool is_quit = false;

	struct order_info {
		int amount;
		int price;
		int index;
	};

	QRCode qrcode;

	// std::string SERVER_ADDRESS = "\"192.168.1.102\"";
	std::string SERVER_ADDRESS = "\"192.168.1.100\"";

	//NHO UPDATE KEYWORD SIZE!!	
	std::string keywords[] = {"Quit", "Order", "Review", "Show QR"};
	std::map<std::string, order_info> order_list;
	
	#include 	"./dialog_new.cpp"

	#include	"./menu_bar_new.cpp"
	RestaurantMenuBar menuBar("MAIN MENU");

	using namespace std;
#endif