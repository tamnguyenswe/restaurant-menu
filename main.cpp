//TODO: clear het tan du pcd8544 cu
//TODO: tach ra ham generate qr string rieng

#include    <wiringPi.h>
// #include    <PCD8544.h>
#include    <iostream>
#include    <string>

#include    "./PCD8544/PCD8544.cpp"
#include    "global_variables.h"
#include    "global_functions.cpp"
#include    "menu/menu.cpp"
#include	"menu_bar_new.cpp"

int main(int argc, char const *argv[]) {
	wiringPiSetup();
	pin_init();
	digitalWrite(BACKLIGHT, ON);

	LCD_inner  =   new LCD();
	LCD_outer  =   new LCD(EXTENDED_SCLK, EXTENDED_DIN, EXTENDED_DC, EXTENDED_CS, EXTENDED_RTS);
	
	menu_init();
	menu_show();

	// LCDclear();
	// LCDdrawstring_P(0 ,0 ,foo.c_str());
	// LCDdisplay();
	// std::cout << "foo";
	return 0;
}