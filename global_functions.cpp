#ifndef _GLOBAL_FUNCS_
#define _GLOBAL_FUNCS_

#include "global_variables.h"

void wait_button(int button) {
    while(digitalRead(button) == OFF) {
        delay(50);
    }
}

bool is_pressed(int btn) {
    return digitalRead(btn) == OFF;
}

void pin_init() {
    pinMode(BTN_1, INPUT);
    pinMode(BTN_2, INPUT);
    pinMode(BTN_3, INPUT);
    pinMode(BTN_4, INPUT);
    pinMode(BACKLIGHT, OUTPUT);

    pullUpDnControl(BTN_1, PUD_UP);
    pullUpDnControl(BTN_2, PUD_UP);
    pullUpDnControl(BTN_3, PUD_UP);
    pullUpDnControl(BTN_4, PUD_UP);
}

#endif